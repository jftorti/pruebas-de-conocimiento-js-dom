'use strict';

// puntuaciones
const puntuaciones = [
  {
    equipo: 'Toros Negros',
    puntos: [1, 3, 4, 2, 10, 8],
  },
  {
    equipo: 'Amanecer Dorado',
    puntos: [8, 5, 2, 4, 7, 5, 3],
  },
  {
    equipo: 'Águilas Plateadas',
    puntos: [5, 8, 3, 2, 5, 3],
  },
  {
    equipo: 'Leones Carmesí',
    puntos: [5, 4, 3, 1, 2, 3, 4],
  },
  {
    equipo: 'Rosas Azules',
    puntos: [2, 1, 3, 1, 4, 3, 4],
  },
  {
    equipo: 'Mantis Verdes',
    puntos: [1, 4, 5, 1, 3],
  },
  {
    equipo: 'Ciervos Celestes',
    puntos: [3, 5, 1, 1],
  },
  {
    equipo: 'Pavos Reales Coral',
    puntos: [2, 3, 2, 1, 4, 3],
  },
  {
    equipo: 'Orcas Moradas',
    puntos: [2, 3, 3, 4],
  },
];

const copiaPuntuaciones = [...puntuaciones];
const totalPuntuaciones = [];

function masMenosPuntos(puntos) {
  for (let puntuacion of copiaPuntuaciones) {
    let sumaPuntosEquipo = 0;
    for (let i = 0; i < puntuacion.puntos.length; i++) {
      sumaPuntosEquipo += puntuacion.puntos[i];
    }
    totalPuntuaciones.push({
      nombre: puntuacion.equipo,
      puntosTotal: sumaPuntosEquipo,
    });
  }
  const equiposOrdenadosMenorMayor = totalPuntuaciones.sort(
    (a, b) => a.puntosTotal - b.puntosTotal
  );
  console.log(
    `El total de puntos del equipo perdedor es ${
      equiposOrdenadosMenorMayor[0].puntosTotal
    } y son los ${equiposOrdenadosMenorMayor[0].nombre} :(,
    y el primer equipo de la lista y ganador es ${
      equiposOrdenadosMenorMayor[equiposOrdenadosMenorMayor.length - 1].nombre
    } !! sumando un total de ${
      equiposOrdenadosMenorMayor[equiposOrdenadosMenorMayor.length - 1]
        .puntosTotal
    } puntos :) !!`
  );
}
masMenosPuntos(puntuaciones);

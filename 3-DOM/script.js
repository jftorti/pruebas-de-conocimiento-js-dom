'use strict';

function UpdateReloj() {
  const date = new Date();
  let relojByClass = document.querySelector('.reloj');
  relojByClass.innerHTML = `${date.getHours()} hs :${date.getMinutes()} min :${date.getSeconds()} seg`;
}

let body = document.querySelector('body');
let reloj = document.createElement('div');
reloj.innerHTML = `<div class="reloj"></div>`;
body.appendChild(reloj);

setInterval(function refresh() {
  for (let i = 0; i < 1; i++) {
    UpdateReloj();
  }
}, 1000);

'use strict';

function getNumber() {
  let intentos = 0;
  do {
    let numRandom = Math.floor(Math.random() * 101); // random entre 0 y 100
    const numUser = parseFloat(
      prompt('Indique un número de usuario: ').toLowerCase()
    );

    if (numRandom !== numUser) {
      intentos++;
      alert(
        numUser > numRandom
          ? 'Número Incorrecto :(. Es MENOR'
          : 'Número Incorrecto :(. Es MAYOR' + `-- Intento Nro ${intentos} de 5`
      );
    } else {
      alert('Has acertado!! :)');
      break;
    }
  } while (intentos < 5); // 5 intentos

  if (intentos === 5) {
    return alert('Has superado el número de intentos...');
  }
}

getNumber();
